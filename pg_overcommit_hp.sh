#!/bin/bash

# Christophe COURTOIS 2023
# GPL v3

usage () {
        printf '%s\n'                                                              \
        "NAME                                                                    " \
        "                                                                        " \
        "  $0 - overcommit & huge pages parameters for PostgreSQL                 " \
        "                                                                        " \
        "SYNOPSIS                                                                " \
        "                                                                        " \
        "  $0 [OPTIONS]  [ > /etc/sysctl.d/999-postgresql.conf ] "\
        "                                                                        " \
        "DESCRIPTION                                                             " \
        "                                                                        " \
        "  Compute overcommit_ratio and Huge Pages for a PG dedicated server.    " \
        "  Parameters are read from /proc/meminfo if not provided in a file,     " \
        "  and read from PostgreSQL if not provided on the command line (provide " \
        "  connection string if necessary)                                       " \
        "  The output is supposed to be fed to 'sysctl -p -' (temporary) or      " \
        "  copied as if it is in sysctl.conf or a similar file                   " \
        "  Standard error contains information and maybe some warnings.          " \
        " " \
        "KNOWN BUGS & LIMITATIONS                                                " \
        "" \
        " Untested on old setups                                                 " \
        " Dependendcy on bc                                                      " \
        " Only 2KB HP pages                                                      " \
        "                                                                        " \
        "OPTIONS                                                                 " \
        "" \
        "  -m, --meminfo    file with /proc/meminfo information                  " \
        "                   (default: local /proc/meminfo)                       " \
        "Memory options (overrides file content)                                 " \
        "  -r, --ram        RAM size  (default: found in /proc/meminfo or -m)    " \
        "  -w, --swap       swap size (default: see --ram)                       " \
        "  -c, --os-cache   OS cache to preserve (as % of memory, default: 20)   " \
        " " \
        "PostgreSQL parameters (if not available, a call to psql will be done):  " \
        "  -b, --shared-buffers, --sb   PostgreSQL shared buffers size           " \
        "  -x, --dbname           PostgreSQL connection string, if necessary     " \
        "                         (only if --share-buffers not provided)         " \
        "                        eg: 'host=server port=5432 dbname=db user=admin'" \
        "  -p, --psql            psql path if not in the PATH                    " \
        "" \
        "NB: all memory values must use B,KB,MB,GB,MB (eg: 1024MB, 32GB)         " \
        "" \
        "Calculations:                                                           " \
        "  -n, --no-hp      no Huge Pages, adapt overcommit                      " \
        "  -i, --hp-limit   no HP if shared_buffers below this (default:4GB)     " \
        "Output:                                                                 " \
        "  -o, --output     output file for sysctl.conf parameters               " \
        "                   (default: standard output)                           " \
        "  -g, --no-colors  no colors in error ouput                             " \
        "  -q, --quiet      quiet (no error output except errors)                " \
        "  -y, --no-comments  no comments in the generated output                " \
        "Other:                                                                  " \
        "  -d                debug mode (overrides -q)                           " \
        "  -h, --help        this help                                           " \
        "                                                                        " \
        "EXAMPLES" \
        "" \
        " pg_overcommit_hp.sh                                                    " \
        " pg_overcommit_hp.sh > /etc/sysctl.d/999-postgresql.conf                " \
        " pg_overcommit_hp.sh | sudo sysctl -p -                                 " \
        " pg_overcommit_hp.sh -x 'port=5433 user=admin dbname=postgres'          " \
        " pg_overcommit_hp.sh --shared-buffers=16GB                              " \
        " pg_overcommit_hp.sh --ram=128GB --swap=1GB --shared-buffers=32GB       " \
        " pg_overcommit_hp.sh -r 32GB -w 0B -b 4196MB --os-cache 30 --no-hp      " \
        " pg_overcommit_hp.sh -r 8GB -w 0B -b 2GB --hp-limit=3GB                 " \
        " pg_overcommit_hp.sh --shared-buffers=16GB -m proc.meminfo.txt          " \
        " pg_overcommit_hp.sh --psql=/opt/bin/psql                               " \
        ""
    exit 1 ;
}

# Todo

# TODO : remove bc dependency
# TODO : read PageTables to detec a problem with TBL
# TODO : detect current HP configuration?
# TODO : test huge_pages = try/yes
# TODO : -l / --hp-limit to set no more than N huge pages (and adapt overcommit)
# TODO : generate sysctl -w  commands? or is the pipe enough?

# Defaults
#
OUTPUT_FILE=/dev/stdout
MEMINFO_FILE=/proc/meminfo
PSQL=${PSQL:-psql}
CNX_STRING=""
SBO=""  # original or pretty
HUGE_PAGES=yes
COLOR=yes
QUIET=no
COMMENTS=yes

HP_MARGIN=110     # security margin for HP computing (TODO : as a parameter)
OS_CACHE_PCT=20        # percentage of OS cache to preserve
HP_MINIO=4GB

# Replace --params=toto by --params toto
# (getopts below does not accept --param=toto, only --param toto)
# And we have --dbname="port=5432 user=postgres"
#
for arg in "$@"; do
  shift
  if [[ "$arg" =~ --[a-z-]*= ]] ; then
      set -- "$@" "${arg%%=*}"   #  --params
      set -- "$@" "${arg#*=}"    #  toto
  else
      set -- "$@" "$arg"         # just repeat
  fi
done
#
# Transform long options to short ones
#
for arg in "$@"; do
  shift
  case "$arg" in
    '--dbname')          set -- "$@" '-x'   ;;
    '--debug')           set -- "$@" '-d'   ;;
    '--help')            set -- "$@" '-h'   ;;
    '--hp-limit')        set -- "$@" '-i'   ;;
    '--hplimit')         set -- "$@" '-i'   ;;
    '--meminfo')         set -- "$@" '-m'   ;;
    '--no-colors')       set -- "$@" '-g'   ;;
    '--nocolors')        set -- "$@" '-g'   ;;
    '--nocolor')         set -- "$@" '-g'   ;;
    '--no-comments')     set -- "$@" '-y'   ;;
    '--nocomments')      set -- "$@" '-y'   ;;
    '--nocomment')       set -- "$@" '-y'   ;;
    '--no-hp')           set -- "$@" '-n'   ;;
    '--nohp')            set -- "$@" '-n'   ;;
    '--no-huge-pages')   set -- "$@" '-n'   ;;
    '--os-cache')        set -- "$@" '-c'   ;;
    '--oscache')         set -- "$@" '-c'   ;;
    '--output')          set -- "$@" '-o'   ;;
    '--psql')            set -- "$@" '-p'   ;;
    '--quiet')           set -- "$@" '-q'   ;;
    '--ram')             set -- "$@" '-r'   ;;
    '--shared-buffers')  set -- "$@" '-b'   ;;
    '--sharedbuffers')   set -- "$@" '-b'   ;;
    '--sb')              set -- "$@" '-b'   ;;
    '--swap')            set -- "$@" '-w'   ;;
    *)                   set -- "$@" "$arg" ;;
  esac
done
while getopts "b:c:i:m:o:p:r:w:x:dhgnqy" option ; do
    case "${option}" in
        m) MEMINFO_FILE="${OPTARG}" ;;
        o) OUTPUT_FILE="${OPTARG}"  ;;
        b) SBO="${OPTARG}"          ;;
        x) CNX_STRING="${OPTARG}"   ;;
        r) MEMTOTALO="${OPTARG}"    ;;
        w) SWAPO="${OPTARG}"        ;;
        c) OS_CACHE_PCT="${OPTARG}" ;;
        n) HUGE_PAGES=no            ;;
        i) HP_MINIO="${OPTARG}"    ;;
        g) COLOR=no                 ;;
        p) PSQL="${OPTARG}"         ;;
        q) QUIET=yes                ;;
        y) COMMENTS=no              ;;
        h) usage '' ; exit 0        ;;
        d) DEBUG=1                  ;;
        ?) usage "ERROR: bad argument"; exit 42 ;;
    esac
done

: ${DEBUG:=0}
[ $DEBUG = 1 ] && QUIET=no

if [ $COLOR = yes ] ; then
    # https://www.shellhacks.com/bash-colors/
    BOLD="\e[1m" ; BLUE="\e[34m" ; GREEN="\e[32m" ; RED="\e[31m" ; PURPLE="\e[35m" ; LGRAY="\e[37m" ; NORMAL="\e[0m"
else
    BOLD="" ; BLUE="" ; GREEN="" ; RED="" ; LGRAY="" ; PURPLE="" ; NORMAL=""
fi
    
debug(){
    [ $DEBUG = 1 ] && ( >&2 printf "${BLUE}${BOLD}DEBUG:${NORMAL}${BLUE} ${1}${NORMAL}\n" ) || true
}
warning(){
    [ $QUIET = no ] && ( >&2 printf "${RED}${BOLD}WARNING:${NORMAL}${RED} ${1}${NORMAL}\n" ) || true
}
info(){
    [ $QUIET = no ] && ( >&2 printf "${GREEN}${BOLD}INFO:${NORMAL}${GREEN} ${1}${NORMAL}\n" ) || true
}
fatal(){
    >&2 printf "${RED}${BOLD}FATAL:${NORMAL}${RED} ${1}${NORMAL}\n" 
    exit 124
}
result(){
    if [ $COMMENTS = yes -o ${1:0:1} != '#' ] ; then
        echo "$1" >> $OUTPUT_FILE
    fi
}

set -o errexit          # exit immediately if a command fails
set -o nounset          # error when substituting an unset variable
set -o pipefail         # return failed rc on pipe commands

# convert a string with unit to a number of KB
# 
conv2KB () {
    local p="$1"
    local n=$(echo "$p"|grep -o -E '^[0-9]*')     # numbers at beginning
    local u=$(echo "$p"|grep -o -E '[a-zA-Z]*B$')  # letters with B at the end
    # debug "$u - $n"
    if [ -z "$u" -o -z "$n" ] ; then
        fatal "Cannot understand size: $p (eg: 1024B, 200KB, 2GB)"
    fi
    case "$u" in
        "B") echo $(( n / 1024 )) ;; 
        "KB") echo "$n" ;; 
        "MB") echo $(( n * 1024 )) ;; 
        "GB") echo $(( n * 1024 *1024 )) ;; 
        "TB") echo $(( n * 1024 * 1024 * 1024)) ;; 
        "PB") echo $(( n * 1024 * 1024 * 1024 *1024 )) ;; 
    esac
}

# formatting
# eg: 1024 MB (1 GB)
# TODO : show at least one decimal for GB; " 2816000 KB ( 2 GB ) " is a bit a problem
#
show () {
    local q=$1   # KB
    if [ $q -gt 1048576 ] ; then
       echo "$q KB ( $(( $q / 1024 / 1024 )) GB )"
    elif [ $q -gt 1024 ] ; then
        echo "$q KB ( $(( $q / 1024 )) MB )"
    else 
        echo "$q KB"
    fi
}

# read data from /proc/meminfo or a similar file 
# (cmd line parameters override)
read_meminfo () {
    [ ! -r "$MEMINFO_FILE" ] && fatal "File $MEMINFO_FILE absent or unreadable"
    debug "Opening $MEMINFO_FILE"
    # MemTotal:       16169964 kB
    if [ -z "${MEMTOTALO:-}" ] ; then
        MEMTOTAL=$(cat "$MEMINFO_FILE" | awk '/^MemTotal/ {print $2}')
    else
        MEMTOTAL=$(conv2KB $MEMTOTALO)
    fi
    [ -z "$MEMTOTAL" ] && fatal "Cannot read MemTotal in ${MEMINFO_FILE}!"
    debug "MemTotal     : $MEMTOTAL"
    if [ -z "${SWAPO:-}" ] ; then
        SWAP=$(cat "$MEMINFO_FILE" | awk '/^SwapTotal/ {print $2}')
    else
        SWAP=$(conv2KB $SWAPO)
    fi
    if [ -z "$SWAP" ] ; then
        warning "Cannot read SwapTotal in ${MEMINFO_FILE}! Assume 0"
        SWAP=0
    fi
    debug "Swap         : $SWAP"
    if [ $HUGE_PAGES = yes ] ; then
        HP_SIZE=$(cat "$MEMINFO_FILE" | awk '/^Hugepagesize/ {print $2}')
        if [ -z "$HP_SIZE" ] ; then
            warning "Cannot read Hugepagesize in ${MEMINFO_FILE}! Assume none"
            HUGE_PAGES=no ; HP_SIZE=2048
        else
            debug "HP size      : $HP_SIZE"
            if [ $HP_SIZE -ne 2048 ] ; then
                fatal "HP <> 2MB : NOT IMPLEMENTED YET!"
            fi
        fi
    fi
}

# How many huge pages
# 
sb2hp () {
    # $1 ex 32000 KB
    # $2 hp_size=$2   # ex 2 KB
    echo $(( $1 * $HP_MARGIN / $2 / 100 ))
}

debug "Meminfo file : $MEMINFO_FILE"
debug "Output file  : $OUTPUT_FILE"
debug "OS cache (%%) : $OS_CACHE_PCT"

if [ $OS_CACHE_PCT -gt 99 -o $OS_CACHE_PCT -lt 1 ] ; then
    fatal "OS cache % must be between 1 and 99 (%)"
fi

HP_MINI=$(conv2KB $HP_MINIO)

read_meminfo

[ $SWAP -gt 2097152 ] && warning "Swap $(show $SWAP) > 2 GB; probably too much on a dedicated DB server"

[ -z "$SBO" ] && debug "PG connection : $CNX_STRING"
if [ ! -z "$SBO" ] ; then
    debug "shared_buffers parameter: $SBO"
else
    debug "Connecting to PG…"
    [ ! -f "$PSQL" -a ! -f "$(which $PSQL)" ] &&  fatal "I need to connect but $PSQL is unavailable!"
    [ -z "$CNX_STRING" ] && warning "No connection string for psql, hoping the default is okay…"
    SBO=$( PGAPPNAME="pg_overcommit_hp.sh" $PSQL "$CNX_STRING" -XAtc 'SHOW shared_buffers' )
    info "shared_buffers found in database: $SBO"
fi
SBK=$(conv2KB "$SBO")

# sanity check
if [ $SBK -ge $MEMTOTAL ] ; then
    fatal "Shared buffers > memory!!"
elif [ $SBK -ge $(( MEMTOTAL * 60 / 100 )) ] ; then
    warning "Shared buffers > 60%% of memory!"
fi

# How many Huge pages?
#
HPNB=0 ; HPTOTSIZE=0
if [ $HUGE_PAGES = yes ] ; then 
    # if shared_buffers too small, avoid using huge pages
    if [ $HP_MINI -le $SBK ] ; then
        HPNB=$(sb2hp "$SBK" "$HP_SIZE")
        HPTOTSIZE=$(( HPNB * $HP_SIZE ))
    else
        info "Huge pages deactivated (minimum $HP_MINI KB ($HP_MINIO) >  shared_buffers ($SBK KB) )"
        HUGE_PAGES=no
    fi
fi
debug "HP total size : $(show $HPTOTSIZE)"

OS_RESERVED_SIZE=$(( $MEMTOTAL * $OS_CACHE_PCT / 100 ))

# overcommit_ratio
# eg : ( 100 - 20 ) * ( 1.0 - 2097152 * 1.0 / 8388608 )
# without HP
# VM_OVERCOMMIT_RATIO_FORMULA="( 100 - $OS_CACHE_PCT ) * ( 1.0 - $SWAP * 1.0 / $MEMTOTAL )"
# with HP
VM_OVERCOMMIT_RATIO_FORMULA="( ( 100 - $OS_CACHE_PCT )/100 * $MEMTOTAL - $SWAP - $HPTOTSIZE ) / ( $MEMTOTAL - $HPTOTSIZE ) *100"
debug "overcommit_ratio formula: $VM_OVERCOMMIT_RATIO_FORMULA"
# beware, bc is very sensitive
# TODO : remove dependendcy to bc
VM_OVERCOMMIT_RATIO=$(echo "$VM_OVERCOMMIT_RATIO_FORMULA"| bc -l |cut -d'.' -f1)

# info and some checks

info "Memory : $(show $MEMTOTAL) / swap : $(show $SWAP) "
info "shared_buffers   : $(show $SBK)"
[ $HUGE_PAGES = yes ] && info "Huge pages number: $HPNB (shared_buffers + security margin : $HP_MARGIN %%) = $(show $HPTOTSIZE) " || info "No Huge pages"
info "OS reserved : $(show $OS_RESERVED_SIZE)"
info "Overcommit_ratio : $VM_OVERCOMMIT_RATIO"

if [ ${VM_OVERCOMMIT_RATIO:-0} -lt 1 ] ; then
    fatal "Overcommit ratio at 0 or below: shared_buffers + OS cache probably too big for the memory, or swap too huge compared to the memory"
fi

COMMITLIMITFORMULA=" $SWAP + ( $MEMTOTAL - $HPTOTSIZE ) * $VM_OVERCOMMIT_RATIO / 100 "
debug "CommitLimit formula : $COMMITLIMITFORMULA"
COMMITLIMIT=$(( $COMMITLIMITFORMULA ))
info "CommitLimit: $(show $COMMITLIMIT)"

# Output
# (lines beginning with # will be masked if  --no-comments 
#
result "# Overcommit configuration"
result "# Parameters:"
result "#  MemTotal: $(show $MEMTOTAL) SwapTotal: $(show $SWAP) OS: $OS_CACHE_PCT %"
result "#  shared_buffers: $SBO (${SBK:?} KB)"
result "# CommitLimit: $(show $COMMITLIMIT)"
result "# OS reserved (cache & system) : $(show $OS_RESERVED_SIZE) "
result "vm.overcommit_memory=2"
result "vm.overcommit_ratio=$VM_OVERCOMMIT_RATIO"
if [ $HUGE_PAGES = yes ] ; then
    result "# HP size : $HP_SIZE KB ; total quantity:  $(show $HPTOTSIZE) (dynamic) "
    # TODO: accept non dynamic if HP size = 1GB
    result "vm.nr_overcommit_hugepages=$HPNB"
    result "vm.nr_hugepages=0"
else
    result "# Huge pages deactivated"
    result "vm.nr_overcommit_hugepages=0"
    result "vm.nr_hugepages=0"
fi
